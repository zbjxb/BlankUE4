// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlankUE4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BLANKUE4_API ABlankUE4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
